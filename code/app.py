from configparser import ConfigParser
from flask import Flask, render_template, request, redirect, jsonify, session
import psycopg2
import psycopg2.extras
from geopy.geocoders import Nominatim

app = Flask(__name__)


def config(filename='DBConfig.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db


@app.route('/', methods=['POST', 'GET'])
def index():
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        if request.method == 'GET':
            cur.execute("Select * from tascas")
            tascas = cur.fetchall()
            return render_template('index.html', tascas=tascas)
        else:
            name = request.form['Name']
            address = request.form['Address']
            rating = request.form['Rating']
            cur.execute("insert into tascas values(%s, %s, %s)", (name, address, rating))
            conn.commit()
            cur.execute("Select * from tascas")
            tascas = cur.fetchall()
            return render_template('index.html', tascas=tascas)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


@app.route('/edit/<name>', methods=['POST', 'GET'])
def edit(name):
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        if request.method == 'GET':
            cur.execute("Select * from tascas where name = %s", (name,))
            tascas = cur.fetchall()[0]
            return render_template('edit.html', tascas=tascas)
        else:
            nName = request.form['Name']
            address = request.form['Address']
            rating = request.form['Rating']
            cur.execute("update tascas set name=%s, address=%s, rating=%s where name=%s",
                        (nName, address, rating, name))
            conn.commit()
            cur.execute("Select * from tascas")
            tascas = cur.fetchall()
            return render_template('index.html', tascas=tascas)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


@app.route('/delete/<name>', methods=['GET'])
def delete(name):
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute("delete from tascas where name=%s", (name,))
        conn.commit()
        cur.execute("Select * from tascas")
        tascas = cur.fetchall()
        return render_template('index.html', tascas=tascas)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


@app.route('/<name>', methods=['GET'])
def details(name):
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("Select * from tascas where name = %s", (name,))
        tascas = cur.fetchall()[0]
        geolocator = Nominatim(user_agent="tascas")
        location = geolocator.geocode(tascas[1]+" Coimbra")
        lat = location.latitude
        lon = location.longitude
        return render_template('details.html', tasca=tascas, lat=lat, lon=lon)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    app.run()
