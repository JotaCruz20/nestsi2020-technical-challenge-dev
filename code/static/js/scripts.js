function Openform() {
    document.getElementById("create").style.visibility = "visible";
}

function Closeform() {
    document.getElementById("Name").value = "";
    document.getElementById("Address").value = "";
    document.getElementById("Rating").value = "";
    document.getElementById("create").style.visibility = "hidden";
}

function validateForm() {
    var name = document.getElementById("Name").value;
    var adrress = document.getElementById("Address").value;
    var rating = document.getElementById("Rating").value;
    var flag = true;
    if (name == "") {
        alert("Name must be filled out");
        flag = false;
    }
    if (adrress == "") {
        alert("Address must be filled out");
        flag = false;
    }
    if (rating == "") {
        alert("Rating must be filled out");
        flag = false;
    }
    return flag;
}

function filter() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('search');
    filter = input.value.toUpperCase();
    ul = document.getElementById("tascas");
    li = ul.getElementsByTagName('tr');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName('th');
        txtValue = a[0].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
